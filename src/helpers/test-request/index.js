import axios from 'axios'

export async function testRequest() {
	try {
		await axios.get('/tasks/1')

		return true
	} catch (err) {
		if (err.response.status === 401) {
			return false
		}
	}
}
