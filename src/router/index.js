import Vue from 'vue'
import VueRouter from 'vue-router'
import { tasksRoutes } from '@/modules/tasks'
import { authRoutes } from '@/modules/auth'
import { errorRoutes } from '@/modules/error'
import { homeRoutes } from '@/modules/home'
import store from '@/store'
import { testRequest } from '@/helpers/test-request'

Vue.use(VueRouter)

const routes = [...homeRoutes, ...authRoutes, ...tasksRoutes, ...errorRoutes]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})

router.beforeEach(async (to, from, next) => {
	const isAuth = store.state.auth.isAuthenticated
	let loggedIn = null

	if (!isAuth) {
		loggedIn = await testRequest()
	} else loggedIn = true

	if (to.name !== 'Auth') {
		if (!isAuth) {
			if (loggedIn) {
				store.state.auth.isAuthenticated = true

				next()
			} else next({ name: 'Auth' })
		} else next()
	} else if (loggedIn) {
		next({ name: 'Home' })
	} else next()
})

export default router
