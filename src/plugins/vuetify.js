import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

const vuetify = new Vuetify({
	theme: {
		themes: {
			light: {
				primary: '#3f51b5',
				secondary: '#2196f3',
				accent: '#f5646c',
				error: '#f44336',
				warning: '#ff5722',
				info: '#eeeeee',
				success: '#4caf50',
			},

			// dark: {
			// 	primary: '#333333',
			// 	secondary: '#6ca2bd',
			// 	accent: '#232834',
			// 	info: '#000',
			// 	success: '#4caf50',
			// },
		},
	},
})

export default vuetify
