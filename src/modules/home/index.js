import Home from './views/main'

export const homeRoutes = [{
	path: '/',
	name: 'Home',
	component: Home,
}]
