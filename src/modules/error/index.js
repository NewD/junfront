export const errorRoutes = [
	{
		path: '*',
		name: 'error-404',
		component: () => import('./views/main'),
	},
]
