import axios from 'axios'

export async function login(data) {
	try {
		const response = await axios.post('/login', data, {
			headers: { 'Content-Type': 'application/json' },
		})

		return response.data
	} catch (err) {
		if (err.response) throw new Error(err.response.data.description)
		else throw new Error(err)
	}
}

export default {
	state: { isAuthenticated: false },

	actions: {
		async signIn(ctx, data) {
			try {
				await login(data)

				ctx.commit('setAuthStatus', true)
			} catch (err) {
				if (err.response) throw new Error(err.response.data.description)
				else throw new Error(err)
			}
		},

		async signUp(ctx, data) {
			try {
				await axios.post('/users', data, {
					headers: { 'Content-Type': 'application/json' },
				})

				await login(data)

				ctx.commit('setAuthStatus', true)
			} catch (err) {
				if (err.response) throw new Error(err.response.data.description)
				else throw new Error(err)
			}
		},

		async logout(ctx) {
			try {
				await axios.get('/logout')

				ctx.commit('setAuthStatus', false)
			} catch (err) {
				console.log(err)
			}
		},
	},

	mutations: {
		setAuthStatus(state, authStatus) {
			state.isAuthenticated = authStatus
		},
	},
}
