import axios from 'axios'

export default {
	state: {
		tasks: [],
		fetchedPages: new Set(),
		amount: 0,
		showCount: 5,
		isChanged: false,
		taskChanges: new Map(),
	},

	actions: {
		async fetchSomeTasks(ctx, { page, count }) {
			try {
				const response = await axios.get(`/tasks?count=${count}&page=${page}`)

				ctx.commit('setTasksPage', { ...response.data, page, count })

				return response.data
			} catch (err) {
				if (err.response) throw new Error(err.response.data.description)
				else throw new Error(err)
			}
		},

		async addTask(ctx, task) {
			try {
				await axios.post('/tasks', task, {
					headers: { 'Content-Type': 'application/json' },
				})

				ctx.commit('addNewTask', task)
			} catch (err) {
				if (err.response) throw new Error(err.response.data.description)
				else throw new Error(err)
			}
		},
	},

	mutations: {
		addNewTask(state, task) {
			state.tasks.push(task)
			state.fetchedPages.clear()
		},

		clearPages(state) {
			state.fetchedPages.clear()
		},

		// ! there is to much logic!!!
		setTasksPage(state, { tasks, amount, page }) {
			const pageStart = state.showCount * (page - 1)
			if (state.tasks.length < pageStart) {
				const nopCount = pageStart - state.tasks.length
				for (let i = 0; i < nopCount; i++) {
					state.tasks.push(null)
				}

				state.tasks.push(...tasks)
			} else if (state.tasks.length === pageStart) {
				state.tasks.push(...tasks)
			} else state.tasks.splice(pageStart, state.showCount, ...tasks)

			state.amount = amount
			state.fetchedPages.add(+page)
		},

		setShowCount(state, count) {
			state.showCount = count
		},

		clearTasks(state) {
			state.tasks = []
		},

		addChange(state, { change, index }) {
			state.taskChanges.set(change.id, { id: change.id, is_checked: change.is_checked })
			state.tasks[index] = { ...state.tasks[index], ...change }
		},

		setChangeStatus(state, status) {
			state.isChanged = status
		},
	},

	getters: {
		getTasks(state) {
			return state.tasks
		},

		getTasksCount(state) {
			return state.amount
		},

		getFetchStatus(state) {
			return state.fetchedPages
		},

		getShowCount(state) {
			return state.showCount
		},

		getChanges(state) {
			return state.taskChanges
		},

		getModifiedTaskId(state) {
			return state.modifiedTasksIds
		},

		getChangeStatus(state) {
			return state.isChanged
		},
	},
}
