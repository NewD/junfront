import TaskList from './components/tasks-list'

export const tasksRoutes = [
	{
		path: '/tasks',
		redirect: '/tasks/1',
	},
	{
		path: '/new-task',
		name: 'TaskConstructor',
		component: () => import('./views/constructor'),
	},
	{
		path: '/tasks/:page',
		name: 'Task-view',
		component: () => import('./views/tasks-view'),

		children: [
			{
				path: '',
				name: 'TaskList',
				component: TaskList,
			},
		],

		beforeEnter(to, from, next) {
			if (+to.params.page) next()
			else next('404')
		},
	},
]
