const DirectoryNamedWebpackPlugin = require('directory-named-webpack-plugin')

module.exports = {
	transpileDependencies: ['vuetify'],

	devServer: {
		proxy: 'http://localhost:8000',
	},

	configureWebpack: {
		resolve: {
			modules: ['components', 'node_modules'],
			extensions: ['.js', '.vue'],
			plugins: [new DirectoryNamedWebpackPlugin(true)],
		},

		
	},
}
